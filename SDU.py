#! /usr/bin/env python3

import sys, os, hashlib

#parameters -r | recursive
#           -f | file to check
#           -d | directory

def _getHash(inFile):
    hasher = hashlib.md5()
    with open(inFile, "rb") as realFile:
        for chunk in iter(lambda: realFile.read(4096), b""):
            hasher.update(chunk)
    fileHashTuple = (hasher.hexdigest(), inFile)
#    print("\nPrinting files with hash [tuples]: ", fileHashTuple, "\n")
    return fileHashTuple 

def _compare(mainFile, compFiles):
    dupeGroup = [mainFile[1]]
    compFiles.remove(mainFile)
    for i in compFiles:
        if mainFile[0] == i[0]:
            dupeGroup.append(i[1])
            compFiles.remove(i)
    if len(dupeGroup) <= 1:
        return []
    else:
#        print("\nPrinting Duplicate Files: ", dupeGroup, "\n")
        print(dupeGroup)
        return dupeGroup

def main():
    hashFileList, pairedFileList = [], []
    targDir = sys.argv[-1]
    try:
        fileList = os.listdir(targDir)
        for i in fileList:
            fullFile = os.path.join(targDir, i)
            hashFileList.append(_getHash(fullFile))
#        print("\nPrinting Duplicate Files, again: ", hashFileList, "\n")
        for i in hashFileList:
            pairedHashList = _compare(i, hashFileList)
    except OSError:
        print("File not found.")


if __name__ == "__main__":
    main()
